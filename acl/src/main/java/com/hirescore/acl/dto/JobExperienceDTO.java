package com.hirescore.acl.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class JobExperienceDTO {

    private String role;
    private String startDate;
    private String endDate;
    private String location;
    private String gap;
}
