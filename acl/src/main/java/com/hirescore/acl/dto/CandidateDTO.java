package com.hirescore.acl.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hirescore.acl.entity.Candidate;
import com.hirescore.acl.entity.EmployeeDetails;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CandidateDTO {

    @JsonProperty("Name")
    private EmployeeDetailsDTO employeeDetailsDTO;
    private List<JobExperienceDTO> jobExperienceDTO;
    private String linkedin;

}
