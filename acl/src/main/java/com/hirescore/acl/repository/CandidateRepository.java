package com.hirescore.acl.repository;

import com.hirescore.acl.entity.Candidate;
import com.hirescore.acl.entity.EmployeeDetailsExtension;
import org.springframework.stereotype.Repository;
import java.io.IOException;
import java.util.List;

@Repository
public interface CandidateRepository {

     List<Candidate> readFile() throws IOException;
     Candidate findCandidateByFormatName(String Name) throws IOException;

     void showCandidateAndGap(Candidate candidate) throws Exception;
     List<EmployeeDetailsExtension> employeeDetailsExtensions() throws Exception;
     EmployeeDetailsExtension findCandidateByPhone(Candidate candidate) throws Exception;
}
