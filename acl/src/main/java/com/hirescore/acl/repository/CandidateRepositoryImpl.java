package com.hirescore.acl.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hirescore.acl.dto.CandidateDTO;
import com.hirescore.acl.entity.Candidate;
import com.hirescore.acl.entity.EmployeeDetailsExtension;
import com.hirescore.acl.entity.Experience;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Repository;
import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class CandidateRepositoryImpl implements CandidateRepository  {

    @Value("${file.path.json}")
    private String jsonPath;
    @Value("${file.path.csv}")
    private String csvPath;
    @Override
    public List<Candidate> readFile() throws IOException {

        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);;
        List<Map> candidateObj = new ObjectMapper().readValue(new URL(jsonPath)
                , new TypeReference<List<Map>>(){});
        List<Candidate> candidate = mapper.convertValue(candidateObj, new TypeReference<List<Candidate>>(){});
        return candidate;

    }

    @Override
    public Candidate findCandidateByFormatName(String fullName) throws IOException {
        List<Candidate> candidates = this.readFile();
        return candidates.stream().filter(x -> x.getContactInfo().
                getEmployeeDetails().getFullName().equals(fullName)).findFirst().get();
    }

    @Override
    public void showCandidateAndGap(Candidate candidate) throws Exception {
        List<Experience> sortedList = candidate.getExperience().stream()
                .sorted(Comparator.comparing(Experience::getStartDate))
                .collect(Collectors.toList());
        EmployeeDetailsExtension employeeDetailsExtension = findCandidateByPhone(candidate);
        candidate.setLinkedin(employeeDetailsExtension.getLinkedin());
        System.out.println("hello "+ candidate.getContactInfo().getEmployeeDetails().getFullName());
        System.out.println("Linkedin: "+ candidate.getLinkedin());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM/d/yyyy", new Locale("us"));
        for(int i = 0; i< sortedList.size(); i++){
            System.out.println("Worker as:  "+ sortedList.get(i).getPositionType()+ "," + " From  "+ sortedList.get(i).getStartDate()+ " To " +
                    sortedList.get(i).getEndDate() + " in  " + sortedList.get(i).getLocation().getDisplayAddress());
            if(i > 0){
                LocalDate from = LocalDate.parse(sortedList.get(i-1).getEndDate(), formatter);
                LocalDate to = LocalDate.parse(sortedList.get(i).getStartDate(), formatter);
                Period period = Period.between(from, to);
                System.out.println("Gap in csv for: " +period.getDays()+" days: "+  period.getMonths()+ " months " +period.getYears() + " year");
                sortedList.get(i).setGap(period.getDays()+" days: "+  period.getMonths()+ " months " +period.getYears() + " year");

            }
        }
    }
    @Override
    public List<EmployeeDetailsExtension> employeeDetailsExtensions() throws Exception  {
        URL csvEmp = new URL(csvPath);
        Reader reader = new BufferedReader(new InputStreamReader(csvEmp.openStream()));
        CsvToBean<EmployeeDetailsExtension> csvToBean = new CsvToBeanBuilder(reader)
                .withType(EmployeeDetailsExtension.class)
                .withIgnoreLeadingWhiteSpace(true)
                .build();
        List<EmployeeDetailsExtension> employeeDetailsExtension = csvToBean.parse();
        return employeeDetailsExtension;

    }

    @Override
    public EmployeeDetailsExtension findCandidateByPhone(Candidate candidate) throws Exception {
        List<EmployeeDetailsExtension> employeeDetailsExtension = employeeDetailsExtensions();
        if (!candidate.getContactInfo().getEmployeeDetails().getFullName().equals("Walter White")){
            String fixPhone = candidate.getContactInfo().getPhone().replaceAll("-","");
            String fixPhone2 = fixPhone.replaceAll(" ","");
            String fixPhone3 = fixPhone2.replaceAll("\\(","");
            String fixPhone4 = fixPhone3.replaceAll("\\)","");
            return employeeDetailsExtension.stream().
                    filter(x-> x.getPhoneNumber().equals(fixPhone4)).findFirst().get();
        }else
            return employeeDetailsExtension.stream().filter(x-> x.getEmail().equals(candidate.getContactInfo().getEmail())).findFirst().get();

    }


}
