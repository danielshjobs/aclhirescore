package com.hirescore.acl.errorhandler;

import com.hirescore.acl.dto.ErrorHandler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.io.IOException;
import java.time.LocalDateTime;

@ControllerAdvice(annotations = RestController.class)
public class CandidateControllerAdvice {

    @ExceptionHandler({ConstraintViolationException.class, ValidationException.class, MethodArgumentNotValidException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorHandler> constraintViolationException(final RuntimeException exception, final HttpServletRequest request) {
        ErrorHandler error = new ErrorHandler();
        error.setTimestamp(LocalDateTime.now());
        error.setMessage(exception.getMessage());
        error.setStatus(HttpStatus.BAD_REQUEST.value());
        error.setUri(request.getRequestURI());
        return ResponseEntity.badRequest().body(error);
    }

    @ExceptionHandler({Exception.class, IOException.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ErrorHandler> internalServerException(final Exception exception, final HttpServletRequest request) {
        ErrorHandler error = new ErrorHandler();
        error.setTimestamp(LocalDateTime.now());
        error.setMessage(exception.getMessage());
        error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setUri(request.getRequestURI());
        return ResponseEntity.badRequest().body(error);
    }

}
