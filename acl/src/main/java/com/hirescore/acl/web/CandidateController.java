package com.hirescore.acl.web;


import com.hirescore.acl.dto.CandidateDTO;
import com.hirescore.acl.service.CandidateService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotBlank;

@RestController
@RequestMapping("api/v1/candidate")
@RequiredArgsConstructor
@Validated
public class CandidateController {

    private final CandidateService candidateService;

    @ApiOperation(value = "View the available candidate" ,response = CandidateDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })

    @GetMapping("{fullName}")
    public CandidateDTO findCandidateByFullName(@PathVariable @NotBlank final String fullName) throws Exception {
        return candidateService.findCandidateByFullName(fullName);
    }

}
