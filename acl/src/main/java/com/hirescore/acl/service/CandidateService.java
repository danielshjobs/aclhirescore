package com.hirescore.acl.service;

import com.hirescore.acl.dto.CandidateDTO;
import com.hirescore.acl.dto.EmployeeDetailsDTO;
import com.hirescore.acl.dto.JobExperienceDTO;
import com.hirescore.acl.entity.Candidate;
import com.hirescore.acl.entity.EmployeeDetailsExtension;
import com.hirescore.acl.repository.CandidateRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CandidateService {

    private final CandidateRepository candidateRepository;

    public CandidateDTO findCandidateByFullName(String fullName) throws Exception {

        Candidate candidate = candidateRepository.findCandidateByFormatName(fullName);
        candidateRepository.showCandidateAndGap(candidate);
        EmployeeDetailsExtension employeeDetailsExtension = candidateRepository.findCandidateByPhone(candidate);
        return mapCandidateToCandidateDTO(candidate, employeeDetailsExtension);
    }


    public CandidateDTO mapCandidateToCandidateDTO(Candidate candidate, EmployeeDetailsExtension employeeDetailsExtension){
        EmployeeDetailsDTO employeeDetailsDTO = new EmployeeDetailsDTO();
        employeeDetailsDTO.setFirstName(candidate.getContactInfo().getEmployeeDetails().getFirstName());
        employeeDetailsDTO.setLastName(candidate.getContactInfo().getEmployeeDetails().getLastName());
        List<JobExperienceDTO> jobExperienceDTO = candidate.getExperience().stream().map(x->
                        new JobExperienceDTO(x.getPositionType(), x.getStartDate(),
                                x.getEndDate(), x.getLocation().getDisplayAddress(), x.getGap())).collect(Collectors.toList());

       return new CandidateDTO(employeeDetailsDTO, jobExperienceDTO, employeeDetailsExtension.getLinkedin());

    }
}
