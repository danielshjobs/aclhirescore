package com.hirescore.acl.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EmployeeDetails {

    @JsonProperty("formatted_name")
    private String fullName;
    @JsonProperty("family_name")
    private String lastName ;
    @JsonProperty("given_name")
    private String firstName;

}
