package com.hirescore.acl.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ContactInfo {
    @JsonProperty("name")
    private EmployeeDetails employeeDetails;
    private String phone;
    private String email;
}


