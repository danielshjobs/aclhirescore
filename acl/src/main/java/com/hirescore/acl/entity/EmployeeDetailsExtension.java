package com.hirescore.acl.entity;

import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@AllArgsConstructor
@RequiredArgsConstructor
@Data
public class EmployeeDetailsExtension {

    @CsvBindByName(column = "Email")
    private String email;
    @CsvBindByName(column = "Phone Number")
    private String phoneNumber;
    @CsvBindByName(column = "Linkedin")
    private String linkedin;
}
