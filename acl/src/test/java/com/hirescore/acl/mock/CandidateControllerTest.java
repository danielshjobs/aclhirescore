package com.hirescore.acl.mock;

import com.hirescore.acl.dto.CandidateDTO;
import com.hirescore.acl.dto.EmployeeDetailsDTO;
import com.hirescore.acl.dto.JobExperienceDTO;
import com.hirescore.acl.service.CandidateService;
import com.hirescore.acl.web.CandidateController;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = CandidateController.class)
@AutoConfigureMockMvc
public class CandidateControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CandidateService candidateService;
    @Test
    public void testfindCandidateByFullName() throws Exception {
        EmployeeDetailsDTO employeeDetailsDTO = new EmployeeDetailsDTO("daniel", "shmuel");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM/d/yyyy", new Locale("us"));
        List<JobExperienceDTO> jobExperienceDTOList = Arrays.asList(new JobExperienceDTO("developer", LocalDate.now().format(formatter),
                LocalDate.now().format(formatter), "israel", "0 days"));
        CandidateDTO candidateDTO = new CandidateDTO(employeeDetailsDTO, jobExperienceDTOList, "https://linkedin.com/in/daniel");
        Mockito.when(candidateService.findCandidateByFullName("daniel")).thenReturn(Optional.of(candidateDTO).get());
        mockMvc.perform(get("/api/v1/candidate/{fullName}","daniel"))
                .andExpect(status().isOk()).andExpect(jsonPath("$.*", Matchers.hasSize(3)))
                .andExpect(jsonPath("$.linkedin", Matchers.equalTo("https://linkedin.com/in/daniel")));
        Mockito.verify(candidateService, times(1)).findCandidateByFullName("daniel");

    }


}
