# ACL pattern - Expose candidate REST API application:


## Getting started

At a high level, ACL's intention is to simplify the business for the micro services by taking care of:( legacy protocol mapping, data transform, legacy architecture, variety entities.) all the mention actions , will be done in our ACL by this approach our micro services will not suffer from any issue like non bounded context & ubiquitous language.

ACL is used like a proxy business for our micro services and will pass to them only the relevant DTO's , with no influence from the provider/customer changes. by this practice our micro service will not be sensitive to integration changes.


## What  ACL including
- ACL including a Rest API application to expose endpoint for transform data file to micro services .
- ACL api base on poular layers by the most popular patterns such as :(entity, DTO, unit test,Repository,Service, error handler...)
- ACL api including variety of artifacts: (Spring web, Acutator, Swagger interface..)
   

## To Install:

 - Java 17
 - Maven 3.1+   
 - IDE recommended intellij comunity (the JDK & Maven should be build-in) 

## Run the app:

- Click Run in the IDE.   

## Run test:

- mvn test or /.mvnw test
 
## Run clean:      

- mvn clean

## Run Package:
   
- mvn Package   

## REST API - endpoints details:

-  Get   /api/vi/candidate/{fullName} - To retrive candidate details.

for your convinience, please assist by swagger-ui to invoke the api, below my swaager path:
 -  http://localhost:8081/swagger-ui/index.html



 




